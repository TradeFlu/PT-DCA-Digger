This is a simple python script for helping to clear DCA logs on ProfitTrailer v2. Feel free to use and add improvements.

**DISCLAIMER:** 
Run this script at your own risk. We will ask questions at each step but if you don't know the answer to any of the questions, then you probably shouldn't be running the script!!!

ABOUT:
If you have a lot of DCA bags, you may want to try digging yourself out of some of those positions to free up some for trade (at a loss).

You have 3 options:
1) Wait and hope the market goes up so that the DCA clears itself.
2) Add more funds to your balance for further DCA buys to reduce the average cost of your pairs until they are in profit and clear.
3) Dig yourself out of your DCA positions.

This script focuses on option 3.

OPTION 3:

Step 1: Start by putting ProfitTrailer into Sell Only Mode. Enter the following into the browser adjusting the IP and Port to suit your setup:

https://localhost:8081/settings/sellOnlyMode?type=&enabled=true

The SOM indicator should now turn red in the bottom right of the ProfitTrailer screen (v2).

Step 2: 

Clone or download this repo somewhere on your computer/server:

```
git clone git@gitlab.com:TradeFlu/PT-DCA-Digger.git
```
OR
```
wget -O - https://gitlab.com/TradeFlu/PT-DCA-Digger/-/archive/master/PT-DCA-Digger-master.zip | unzip -
```

Then
```
cd PT-DCA-Digger
```

Step 3:

I like pipenv myself but you can use whatever method you want to install requirements.txt. As always, run in a virtual environment where possible.

```
pipenv install
```
OR
```
pip install -r requirements
```

Step 4:

Run the script and answer the prompts!
```
python dig.py
```

If you like this script and it heps dig you out of your hole, please feel free to make a small donation.

BTC: 3HXbjkf9fUT1rKr9ZEGegQ5tmvHefuVqt8

<img src="/img/Bitcoin_QR_code.png" width="200" height="200">

ETH: 0xBD2161F69e4f80f1818F62c073002BbAC915c910

<img src="/img/Ethereum_QR_code.png" width="200" height="200">

LTC: LToQit5H4LwT2FknePuP61G4i4FGwgZsBX

<img src="/img/Litecoin_QR_code.png" width="200" height="200">

BCH: qzc30rqgd6lnmk35q5mhs0f09h3y6st6kvs869rk2a

<img src="/img/BitcoinCash_QR_code.png" width="200" height="200">