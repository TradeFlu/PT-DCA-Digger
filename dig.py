import decimal
import sys
from operator import itemgetter

import ccxt
import click
import requests


@click.command()
@click.option('--profit_trailer_instance',
              prompt='Please enter your ProfitTrailer instances address',
              default='http://localhost:8081',
              help='The script uses this to grab your DCA logs from your ProfitTrailer instance.')
@click.option('--api_token',
              prompt='Please enter your ProfitTrailer server.api_token from application.properties',
              hide_input=True,
              help='The script uses this to grab your DCA logs from your ProfitTrailer instance.')
@click.option('--default_api_key',
              prompt='Please enter your exchange default_api_key from application.properties',
              hide_input=True,
              help='The script uses this to connect with your exchange.')
@click.option('--default_api_secret',
              prompt='Please enter your exchange default_api_secret from application.properties',
              hide_input=True,
              help='The script uses this to connect with your exchange.')
def start(profit_trailer_instance, api_token, default_api_key, default_api_secret):
    data_url = '{}/api/data?token={}'.format(profit_trailer_instance, api_token)
    dca_url = '{}/api/dca/log?token={}'.format(profit_trailer_instance, api_token)
    som_url = '{}/settings/sellOnlyMode?type=&enabled=true&token={}'.format(profit_trailer_instance, api_token)

    try:
        r = requests.get(data_url)
        data = r.json()
    except:
        click.echo('Failed to connect to PT. Try opening {} in a browser to make sure its correct.'.format(dca_url))
        sys.exit()

    market = data['market']
    exchange = data['exchange']
    click.echo('Base currency: {} on Exchange: {}'.format(market, exchange))
    dca_current_value = data['totalDCACurrentValue']

    api = create_api(exchange, default_api_key, default_api_secret)

    if click.confirm('Put PT into Sell Only Mode (SOM) by visiting {} in your browser. Done?'.format(som_url)):
        click.echo('Connecting to your ProfitTrailer instance')

        try:
            r = requests.get(dca_url)
            dca_log = r.json()
        except:
            click.echo('Failed to connect to PT. Try opening {} in a browser to make sure its correct.'.format(dca_url))
            sys.exit()

        click.echo('You have {} pairs in DCA totalling {} {}. We will start with the one with the least loss.'.format(
            len(dca_log), dca_current_value, market)
        )

        pairs = process_pairs(dca_log)
        for pair in pairs:
            if exchange == 'POLONIEX':
                symbol = '{}/{}'.format(pair['market'].split('_')[1], market)
            else:
                symbol = '{}/{}'.format(pair['market'].split(market)[0], market)
            amount = pair['totalAmount']
            avg_price = pair['avgPrice']
            click.echo('{}: [Bought: {} times] [Avg. Price: {}] [Total Cost: {}] [Profit: {}]'.format(
                symbol,
                pair['boughtTimes'],
                float_to_str(avg_price),
                pair['totalCost'],
                pair['profit']
            ))
            if click.confirm('Would you like to sell this pair or go to next?'):
                if click.confirm('Sell immediately at the current market rate?'):
                    sell_now(api, symbol, amount=amount, market_price=True)
                else:
                    profit = click.prompt('OK, how much is an acceptable loss in %?', type=float, default=1.5)
                    price = avg_price * (1 - (profit / 100))
                    if click.confirm('Would you like to make a sell order now at {}. Please confirm.'.format(
                            float_to_str(price))):
                        sell_now(api, symbol, amount=amount, price=price)
                    else:
                        price = click.prompt('Please enter the exact price you would like to sell at.', type=float,
                                             default=float_to_str(price))
                        if click.confirm(
                                'Would you like to make a sell order now at {}. Please confirm.'.format(price)):
                            sell_now(api, symbol, amount=amount, price=price)

    else:
        click.echo('PT should be in Sell Only Mode. Exiting.')


def create_api(exchange, default_api_key, default_api_secret):
    api = None
    if exchange == 'BINANCE':
        api = ccxt.binance({
            'apiKey': default_api_key,
            'secret': default_api_secret,
        })
    elif exchange == 'BITTREX':
        api = ccxt.bittrex({
            'apiKey': default_api_key,
            'secret': default_api_secret,
        })
    elif exchange == 'POLONIEX':
        api = ccxt.poloniex({
            'apiKey': default_api_key,
            'secret': default_api_secret,
        })
    return api


def process_pairs(dca_log):
    pairs = []
    for dca in dca_log:
        pairs.append({
            'market': dca['market'],
            'boughtTimes': dca['boughtTimes'],
            'profit': dca['profit'],
            'totalCost': dca['averageCalculator']['totalCost'],
            'totalAmount': dca['averageCalculator']['totalAmount'],
            'avgPrice': dca['averageCalculator']['avgPrice']
        })

    return sorted(pairs, key=itemgetter('profit'), reverse=True)


def sell_now(api, symbol, amount=0, market_price=False, price=None):
    if market_price:
        order = api.create_market_sell_order(symbol=symbol, amount=amount)
        click.echo(order)
    else:
        order = api.create_limit_sell_order(symbol, amount, price)
        click.echo(order)


def float_to_str(f):
    """
    Convert the given float to a string,
    without resorting to scientific notation
    """
    ctx = decimal.Context()
    ctx.prec = 20
    d1 = ctx.create_decimal(repr(f))
    return format(d1, 'f')


if __name__ == '__main__':
    start()
